import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import ilabquality.www.CareersPage;
import ilabquality.www.HomePage;
import ilabquality.www.SouthAfricaPage;
import org.junit.Assert;

public class ApplySteps extends BaseClass {

    private SouthAfricaPage southAfricaPage;

    @Given("^As a user I launch the iLabQuality web page$")
    public void asAUserILaunchTheILabQualityWebPage() {
        openBrowser(endPoint);
    }

    @When("^I click Careers$")
    public void iClickCareers() {
        HomePage homePage = new HomePage(driver);
        Assert.assertTrue("Unable to click Careers", homePage.clickCareersTab());
    }

    @And("^I click South Africa$")
    public void iClickSouthAfrica() {
        CareersPage careersPage = new CareersPage(driver);
        Assert.assertTrue("Unable to click South Africa", careersPage.clickSouthAfricaOption());
    }

    @And("^I click first job opening$")
    public void iClickFirstJobOpening() {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to click First Job Opening", southAfricaPage.clickFirstJobOpening());
    }

    @And("^I click Apply Online button$")
    public void iClickApplyOnlineButton() {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to click Apply Online", southAfricaPage.clickApplyOnlineButton());
    }

    @And("^I capture Name \"([^\"]*)\"$")
    public void iCaptureName(String name) {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to capture Name", southAfricaPage.captureNameField(name));
    }

    @And("^I capture Email \"([^\"]*)\"$")
    public void iCaptureEmail(String email) {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to capture Email", southAfricaPage.captureEmailField(email));
    }

    @And("^I capture Phone$")
    public void iCapturePhone() {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to capture Phone", southAfricaPage.capturePhoneField(southAfricaPage.generatePhone()));
    }

    @And("^I click Send Application button$")
    public void iClickSendApplicationButton() {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to click Send Application", southAfricaPage.clickSendApplicationButton());
    }

    @Then("^I verify error message$")
    public void iVerifyErrorMessage() {
        southAfricaPage = new SouthAfricaPage(driver);
        Assert.assertTrue("Unable to display error message", southAfricaPage.verifyErrorMessage());
        driver.quit();
    }
}
