package ilabquality.www;

import ilabquality.common.CommonMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CareersPage extends CommonMethods {

    public CareersPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@href='/careers/south-africa/']")
    private WebElement optionSouthAfrica;

    public boolean clickSouthAfricaOption() {
        if(waitForElementToBeVisible(optionSouthAfrica))
        {
            clickElement(optionSouthAfrica);
            return true;
        }
        return false;
    }
}
