package ilabquality.www;

import ilabquality.common.CommonMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends CommonMethods {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@href='https://www.ilabquality.com/careers/']")
    private WebElement tabCareers;

    public boolean clickCareersTab() {
        if(waitForElementToBeVisible(tabCareers))
        {
            clickElement(tabCareers);
            return true;
        }
        return false;
    }

}
