package ilabquality.www;

import ilabquality.common.CommonMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Random;

public class SouthAfricaPage extends CommonMethods {

    public SouthAfricaPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@href='https://www.ilabquality.com/job/bsc-computer-science-national-diploma-it-development-graduates/']")
    private WebElement firstJobOpening;

    @FindBy(xpath = "//*[@href='https://www.ilabquality.com/job/bsc-computer-science-national-diploma-it-development-graduates/?form=apply#wpjb-scroll']")
    private WebElement btnApplyOnline;

    @FindBy(id = "applicant_name")
    private WebElement txtName;

    @FindBy(id = "email")
    private WebElement txtEmail;

    @FindBy(id = "phone")
    private WebElement txtPhone;

    @FindBy(id = "wpjb_submit")
    private WebElement btnSendApplication;

    @FindBy(xpath = "//*[text()='You need to upload at least one file.']")
    private WebElement errMessage;

    public boolean clickFirstJobOpening() {
        if(waitForElementToBeVisible(firstJobOpening))
        {
            clickElement(firstJobOpening);
            return true;
        }
        return false;
    }

    public boolean clickApplyOnlineButton() {
        if(waitForElementToBeVisible(btnApplyOnline))
        {
            clickElement(btnApplyOnline);
            return true;
        }
        return false;
    }

    public boolean captureNameField(String name) {
        if(waitForElementToBeVisible(txtName))
        {
            captureText(txtName, name);
            return true;
        }
        return false;
    }

    public boolean captureEmailField(String email) {
        if(waitForElementToBeVisible(txtEmail))
        {
            captureText(txtEmail, email);
            return true;
        }
        return false;
    }

    public boolean capturePhoneField(String phone) {
        if(waitForElementToBeVisible(txtPhone))
        {
            captureText(txtPhone, phone);
            return true;
        }
        return false;
    }

    public String generatePhone() {
        int num2;
        int num3;
        int set2;
        int set3;

        Random random = new Random();

        num2 = random.nextInt(8);
        num3 = random.nextInt(8);

        set2 = random.nextInt(999) + 100;

        set3 = random.nextInt(9999) + 1000;

        return "0" + num2 + num3 + " " + set2 + " " + set3;
    }

    public boolean clickSendApplicationButton() {
        if(waitForElementToBeVisible(btnSendApplication))
        {
            clickElement(btnSendApplication);
            return true;
        }
        return false;
    }

    public boolean verifyErrorMessage() {
        if(waitForElementToBeVisible(errMessage))
        {
            return true;
        }
        return false;
    }

}
